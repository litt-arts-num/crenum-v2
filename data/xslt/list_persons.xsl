<?xml version="1.0" encoding="UTF-8"?>
    
 <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xs tei" version="2.0">
        
     <xsl:output method="text" encoding="UTF-8"/>
     
     <!-- SUPPRESS SPACES -->
     <xsl:template match="*/text()[normalize-space()]">
         <xsl:value-of select="normalize-space()"/>
     </xsl:template>
     <xsl:template match="*/text()[not(normalize-space())]"/>
     
     <xsl:template match="tei:teiHeader"/>
     
     <xsl:template match="tei:listPerson">
         <xsl:if test="@type='maximal'">
            <!-- CONVERT INPUT TO JSON -->
            <xsl:variable name="persons">
                <xsl:text>[</xsl:text>
                <xsl:for-each select="tei:person">
                    <xsl:text>{"</xsl:text>
                    <xsl:text>id":"</xsl:text>
                    <string key="id">
                        <xsl:value-of select="@xml:id"/>
                    </string>
                    <xsl:text>","name":"</xsl:text>
                    <string key="name">
                        <xsl:value-of select="tei:persName[@type='normalise']"/>
                    </string>
                    <xsl:text>"}</xsl:text>
                    <xsl:if test="not(position()=last())">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
                </xsl:for-each>
                <xsl:text>]</xsl:text>
            </xsl:variable>
             <!-- OUTPUT -->
             <xsl:value-of select="$persons"/>
         </xsl:if>
     </xsl:template>
     
     <xsl:template match="tei:listRelation"/>
     
     <xsl:template match="tei:text"/>

</xsl:stylesheet>
