<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xs tei" version="2.0">
    
    <xsl:output method="text" encoding="UTF-8"/>
    
    <!-- SUPPRESS SPACES -->
    <xsl:template match="*/text()[normalize-space()]">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>
    <xsl:template match="*/text()[not(normalize-space())]"/>
    
    <xsl:template match="tei:teiHeader"/>
    
    <xsl:template match="tei:listPerson"/>
    
    <xsl:template match="tei:listRelation">
        <!-- CONVERT INPUT TO JSON -->
        <xsl:variable name="relations">
            <xsl:text>[</xsl:text>
            <xsl:for-each select="tei:relation[@name='precede']">
                <xsl:text>{"</xsl:text>
                <xsl:text>id":"</xsl:text>
                <string key="id">
                    <xsl:value-of select="concat(substring-after(@active,'#'),'|',substring-after(@passive,'#'))"/>
                </string>
                <xsl:text>","source":"</xsl:text>
                <string key="source">
                    <xsl:value-of select="substring-after(@active,'#')"/>
                </string>
                <xsl:text>","target":"</xsl:text>
                <string key="target">
                    <xsl:value-of select="substring-after(@passive,'#')"/>
                </string>
                <xsl:text>","type":"</xsl:text>
                <string key="type">
                    <xsl:value-of select="@type"/>
                </string>
                <xsl:text>","name":"</xsl:text>
                <string key="name">
                    <xsl:value-of select="tei:desc"/>
                </string>
                <xsl:text>"}</xsl:text>
                <xsl:if test="not(position()=last())">
                    <xsl:text>,</xsl:text>
                </xsl:if>
            </xsl:for-each>
            <xsl:text>]</xsl:text>
        </xsl:variable>
        <!-- OUTPUT -->
        <xsl:value-of select="$relations"/>
    </xsl:template>
    
    <xsl:template match="tei:text"/>
    
</xsl:stylesheet>
