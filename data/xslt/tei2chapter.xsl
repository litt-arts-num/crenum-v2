<?xml version="1.0" encoding="UTF-8"?><!--
/**

* XSLT for transformation of TEI to HTML : one page by book, chapter by chapter
* @author AnneGF@CNRS
* @date : 2022-2023
*/
-->

<!DOCTYPE tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">
    <xsl:include href="check.xsl"/>

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:variable name="img_width" select="1200"/>
    <xsl:variable name="sides">left right</xsl:variable>
    <xsl:strip-space elements="tei:choice"/>
    <!--<xsl:preserve-space elements="*"/>-->
    <!--<xsl:preserve-space elements="tei:ab tei:bibl tei:note tei:title"/>-->

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">
        <xsl:variable name="tei" select="current()"/>
        <xsl:call-template name="check">
            <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
        </xsl:call-template>

        <xsl:for-each select="tei:TEI/tei:text/tei:body//tei:div[@type = 'book']">
            <xsl:sort select="@n" data-type="number"/>
            <xsl:variable name="book" select="@n"/>
            <xsl:for-each select="./tei:div">
                <xsl:variable name="chap_num" as="xs:integer">
                    <xsl:number select="." count="tei:div" level="single"/>
                </xsl:variable>
                <xsl:variable name="chap">
                    <xsl:value-of select="$chap_num"/>
                    <xsl:text>_</xsl:text>
                    <xsl:value-of select="@type"/>
                    <xsl:if test="@n">
                        <xsl:text>_</xsl:text>
                        <xsl:value-of select="@n"/>
                    </xsl:if>
                </xsl:variable>
                <xsl:variable name="output">
                    <xsl:text>../../templates/htm/</xsl:text>
                    <xsl:value-of select="$book"/>
                    <xsl:text>/</xsl:text>
                    <xsl:value-of select="$chap"/>
                    <xsl:text>.htm.j2</xsl:text>
                </xsl:variable>
                <xsl:result-document method="html" indent="yes" encoding="UTF-8"
                    omit-xml-declaration="yes" href="{$output}">
                    <xsl:call-template name="create_title_book"/>
                    <xsl:call-template name="create_prev_book"/>
                    <xsl:call-template name="create_next_book"/>
                    <xsl:call-template name="generate_chapter">
                        <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
                        <xsl:with-param name="book_num" select="$book"/>
                        <xsl:with-param name="chap_num" select="$chap_num" as="xs:integer"/>
                    </xsl:call-template>
                </xsl:result-document>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>

    <!-- Create title block -->
    <xsl:template name="create_title_book">
        <xsl:text>
{% macro chapterTitle() %}</xsl:text>
        <xsl:choose>
            <xsl:when test="@type = 'chap'">
                <xsl:text>Chapitre </xsl:text>
                <xsl:value-of select="@n"/>
            </xsl:when>
            <xsl:when test="@type = 'prologueProse'">
                <xsl:text>Prologue en prose</xsl:text>
            </xsl:when>
            <xsl:when test="@type = 'prologueVerse'">
                <xsl:text>Prologue en vers</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@type"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>{% endmacro %}
</xsl:text>
    </xsl:template>
    <!-- Create prev block -->
    <xsl:template name="create_prev_book">
        <xsl:text>
{% macro prev() %}</xsl:text>
        <xsl:if test="position() > 1">
            <xsl:variable name="prev_chapter">
                <xsl:text>prevChap('</xsl:text>
                <xsl:value-of select="position() - 1"/>
                <xsl:text>_</xsl:text>
                <xsl:value-of select="preceding::tei:div[1]/@type"/>
            </xsl:variable>
            <xsl:variable name="prev_chapter_id">
                <xsl:choose>
                    <xsl:when test="preceding::tei:div[1]/@type = 'chap'">
                        <xsl:value-of select="$prev_chapter"/>
                        <xsl:text>_</xsl:text>
                        <xsl:value-of select="preceding::tei:div[1]/@n"/>
                        <xsl:text>')</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$prev_chapter"/>
                        <xsl:text>')</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <button type="button" class="btn btn-link">
                <xsl:attribute name="onclick">
                    <xsl:value-of select="$prev_chapter_id"/>
                </xsl:attribute>
                <i class="fa-solid fa-arrow-left"/>
                <xsl:text> </xsl:text>
                <xsl:choose>
                    <xsl:when test="preceding::tei:div[1]/@type = 'chap'">
                        <xsl:text>Chapitre </xsl:text>
                        <xsl:value-of select="preceding::tei:div[1]/@n"/>
                    </xsl:when>
                    <xsl:when test="preceding::tei:div[1]/@type = 'prologueProse'">
                        <xsl:text>Prologue en prose</xsl:text>
                    </xsl:when>
                    <xsl:when test="preceding::tei:div[1]/@type = 'prologueVerse'">
                        <xsl:text>Prologue en vers</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="preceding::tei:div[1]/@type"/>
                    </xsl:otherwise>
                </xsl:choose>
            </button>
        </xsl:if>
        <xsl:text>{% endmacro %}
</xsl:text>
    </xsl:template>

    <!-- Create next block -->
    <xsl:template name="create_next_book">
        <xsl:text>
{% macro next() %}</xsl:text>
        <xsl:if test="not(position() = last())">
            <xsl:variable name="next_chapter">
                <xsl:text>nextChap('</xsl:text>
                <xsl:value-of select="position() + 1"/>
                <xsl:text>_</xsl:text>
                <xsl:value-of select="following::tei:div[1]/@type"/>
            </xsl:variable>
            <xsl:variable name="next_chapter_id">
                <xsl:choose>
                    <xsl:when test="following::tei:div[1]/@type = 'chap'">
                        <xsl:value-of select="$next_chapter"/>
                        <xsl:text>_</xsl:text>
                        <xsl:value-of select="following::tei:div[1]/@n"/>
                        <xsl:text>')</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$next_chapter"/>
                        <xsl:text>')</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <button type="button" class="btn btn-link">
                <xsl:attribute name="onclick">
                    <xsl:value-of select="$next_chapter_id"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="following::tei:div[1]/@type = 'chap'">
                        <xsl:text>Chapitre </xsl:text>
                        <xsl:value-of select="following::tei:div[1]/@n"/>
                    </xsl:when>
                    <xsl:when test="following::tei:div[1]/@type = 'prologueProse'">
                        <xsl:text>Prologue en prose</xsl:text>
                    </xsl:when>
                    <xsl:when test="following::tei:div[1]/@type = 'prologueVerse'">
                        <xsl:text>Prologue en vers</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="following::tei:div[1]/@type"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <i class="fa-solid fa-arrow-right"/>
            </button>
        </xsl:if>
        <xsl:text>{% endmacro %}
</xsl:text>
    </xsl:template>

    <!-- Create chapter view -->
    <xsl:template name="generate_chapter">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="book_num" required="yes"/>
        <xsl:param name="chap_num" required="yes" as="xs:integer"/>
        <xsl:variable name="note" as="xs:boolean">
            <xsl:choose>
                <xsl:when
                    test="count($tei//tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]//tei:note) > 0"
                    >true</xsl:when>
                <xsl:otherwise>false</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="create_tabs">
            <xsl:with-param name="tei" select="$tei" tunnel="yes"/>
            <xsl:with-param name="note" as="xs:boolean" select="$note" tunnel="yes"/>
            <xsl:with-param name="book_num" select="$book_num" tunnel="yes"/>
            <xsl:with-param name="chap_num" select="$chap_num" tunnel="yes" as="xs:integer"/>
        </xsl:call-template>
    </xsl:template>

    <!-- Create tabs contents -->
    <xsl:template name="create_tabs">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="note" as="xs:boolean" tunnel="yes"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>

        <xsl:call-template name="create_block_img"/>
        <xsl:call-template name="create_block_diplo"/>
        <xsl:call-template name="create_block_linear"/>
        <xsl:call-template name="create_block_noteExist"/>
        <xsl:call-template name="create_block_note"/>
        <xsl:call-template name="create_block_collation"/>
    </xsl:template>

    <!-- Img -->
    <xsl:template name="create_block_img">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:text>
{% macro img(pos) %}
</xsl:text>
        <div class="osd-pane tab-pane fade" role="tabpanel" tabindex="0">
            <xsl:attribute name="id">
                <xsl:text>image-{{pos}}</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="aria-labelledby">
                <xsl:text>image-{{pos}}-tab</xsl:text>
            </xsl:attribute>
            <div class="osd-pane-content">
                <xsl:attribute name="id">
                    <xsl:text>osd-pane-content-{{pos}}</xsl:text>
                </xsl:attribute>
                <div class="img-thumbnails">
                    <!--Si la première page d'un chapitre est sur le facsimilé du chapitre précédent-->
                    <xsl:for-each
                        select="$tei/tei:TEI/tei:text/tei:body/tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]">
                        <xsl:if test="not(descendant::*[1][self::tei:pb])">
                            <xsl:for-each select="preceding::tei:pb[1]">
                                <xsl:variable name="data-prefix"
                                    select="replace(@facs, '(.*)/ark:/([^/]+/).*', '$1/iiif/ark:/$2')"/>
                                <xsl:variable name="data-suffix"
                                    select="concat('/full/', $img_width, ',/0/native.jpg')"/>
                                <xsl:variable name="data-ident"
                                    select="replace(@facs, '.*/ark:/[^/]+/(.*).item', '$1')"/>
                                <xsl:variable name="id"
                                    select="replace(@facs, '.*/(.*).item', '$1')"/>
                                <img class="img-thumbnail"
                                    src="{$data-prefix}{$data-ident}/full/,80/0/color.jpg"
                                    data-img="{$data-prefix}{$data-ident}/full/1600,/0/color.jpg"
                                    data-credit="Source : BnF / Gallica">
                                    <xsl:attribute name="id">
                                        <xsl:text>image-{{pos}}-</xsl:text>
                                        <xsl:value-of select="$id"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-pos">
                                        <xsl:text>{{pos}}</xsl:text>
                                    </xsl:attribute>
                                </img>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:for-each
                        select="$tei/tei:TEI/tei:text/tei:body/tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]/descendant::tei:pb">
                        <xsl:variable name="data-prefix"
                            select="replace(@facs, '(.*)/ark:/([^/]+/).*', '$1/iiif/ark:/$2')"/>
                        <xsl:variable name="data-suffix"
                            select="concat('/full/', $img_width, ',/0/native.jpg')"/>
                        <xsl:variable name="data-ident"
                            select="replace(@facs, '.*/ark:/[^/]+/(.*).item', '$1')"/>
                        <xsl:variable name="id" select="replace(@facs, '.*/(.*).item', '$1')"/>
                        <img class="img-thumbnail"
                            src="{$data-prefix}{$data-ident}/full/,80/0/color.jpg"
                            data-img="{$data-prefix}{$data-ident}/full/1600,/0/color.jpg"
                            data-credit="Source : BnF / Gallica">
                            <xsl:attribute name="id">
                                <xsl:text>image-{{pos}}-</xsl:text>
                                <xsl:value-of select="$id"/>
                            </xsl:attribute>
                            <xsl:attribute name="data-pos">
                                <xsl:text>{{pos}}</xsl:text>
                            </xsl:attribute>
                        </img>
                    </xsl:for-each>
                </div>
                <div class="osd-btn text-center">
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-zoom-in-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-search-plus"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-zoom-out-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-search-minus"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-home-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-home"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-full-page-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-arrows-alt"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-left-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-undo"/>
                    </span>
                    <span>
                        <xsl:attribute name="id">
                            <xsl:text>osd-right-{{pos}}</xsl:text>
                        </xsl:attribute>
                        <i class="fa fa-redo-alt"/>
                    </span>
                </div>
                <div class="openseadragon">
                    <xsl:attribute name="id">
                        <xsl:text>openseadragon-{{pos}}</xsl:text>
                    </xsl:attribute>
                </div>
            </div>
        </div>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <!-- Diplo -->
    <xsl:template name="create_block_diplo">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:text>
{% macro diplo(pos) %}</xsl:text>
        <xsl:for-each
            select="$tei/tei:TEI/tei:text/tei:body/tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]">
            <xsl:variable name="id" select="concat($book_num, '-', $chap_num)"/>
            <div class="diplo tab-pane fade" role="tabpanel" tabindex="0">
                <xsl:attribute name="id">
                    <xsl:text>diplo-{{pos}}</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="aria-labelledby">
                    <xsl:text>diplo-{{pos}}-tab</xsl:text>
                </xsl:attribute>
                <xsl:apply-templates mode="diplo"/>
            </div>
        </xsl:for-each>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <!-- Linear -->
    <xsl:template name="create_block_linear">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:text>
{% macro linear(pos) %}</xsl:text>
        <xsl:for-each
            select="$tei/tei:TEI/tei:text/tei:body/tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]">
            <xsl:variable name="id" select="concat($book_num, '-', $chap_num)"/>
            <div class="linear tab-pane fade" role="tabpanel" tabindex="0">
                <xsl:attribute name="id">
                    <xsl:text>linear-{{pos}}</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="aria-labelledby">
                    <xsl:text>linear-{{pos}}-tab</xsl:text>
                </xsl:attribute>
                <div>
                    <span class="btn toggle-mark" data-type="highlight">
                        <xsl:attribute name="data-pos">
                            <xsl:text>{{pos}}</xsl:text>
                        </xsl:attribute>
                        <span class="text-show">
                            <xsl:text>Afficher les surlignages</xsl:text>
                        </span>
                        <span class="text-hide">
                            <xsl:text>Masquer les surlignages</xsl:text>
                        </span>
                    </span>
                    <span class="btn toggle-mark" data-type="collation">
                        <xsl:attribute name="data-pos">
                            <xsl:text>{{pos}}</xsl:text>
                        </xsl:attribute>
                        <span class="text-show">
                            <xsl:text>Afficher les appels de collations</xsl:text>
                        </span>
                        <span class="text-hide">
                            <xsl:text>Masquer les appels de collations</xsl:text>
                        </span>
                    </span>
                    <span class="btn toggle-mark" data-type="note">
                        <xsl:attribute name="data-pos">
                            <xsl:text>{{pos}}</xsl:text>
                        </xsl:attribute>
                        <span class="text-show">
                            <xsl:text>Afficher les appels de notes</xsl:text>
                        </span>
                        <span class="text-hide">
                            <xsl:text>Masquer les appels de notes</xsl:text>
                        </span>
                    </span>
                </div>
                <hr class="btnLine"/>
                <xsl:apply-templates mode="linear"/>
            </div>
        </xsl:for-each>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <!-- Note -->
    <xsl:template name="create_block_noteExist">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="note" as="xs:boolean" tunnel="yes"/>
        <xsl:text>
                {% macro noteExist() %}</xsl:text>
        <xsl:choose>
            <xsl:when test="$note">
                <xsl:text>true</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>false</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>{% endmacro %}
            </xsl:text>
    </xsl:template>

    <xsl:template name="create_block_note">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:text>
            {% macro notes(pos) %}</xsl:text>
        <div class="notes tab-pane fade" role="tabpanel" tabindex="0">
            <xsl:attribute name="id">
                <xsl:text>notes-{{pos}}</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="aria-labelledby">
                <xsl:text>notes-{{pos}}-tab</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates mode="note"
                select="$tei//tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]//tei:note"
            />
        </div>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <xsl:template name="create_block_collation">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:text>
            {% macro collations(pos) %}</xsl:text>
        <div class="collations tab-pane fade" role="tabpanel" tabindex="0">
            <xsl:attribute name="id">
                <xsl:text>collations-{{pos}}</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="aria-labelledby">
                <xsl:text>collations-{{pos}}-tab</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates mode="collation"
                select="$tei//tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]//tei:app"
            />
        </div>
        <xsl:text>
{% endmacro %}
</xsl:text>
    </xsl:template>

    <!-- Template autofermant qui permet, pour un élément donné, de ne rien faire -->
    <xsl:template match="tei:teiHeader"/>

    <xsl:template match="tei:pb" mode="diplo linear">
        <xsl:variable name="id" select="replace(@facs, '.*/(.*).item', '$1')"/>
        <hr data-img-id="{$id}"/>
    </xsl:template>

    <!--En mode linear on n'agit sur le numéro de folio que s'il contient une note-->
    <xsl:template match="tei:fw[@type = 'folio']" mode="linear"/>
    <xsl:template match="tei:fw[@type = 'folio']" mode="diplo">
        <p title="Numéro de {@type}" class="text-end">
            <i>
                <xsl:value-of select="@n"/>
            </i>
        </p>
    </xsl:template>
    <xsl:template match="tei:fw[@type = 'header']" mode="diplo">
        <span class="d-block fw-bold mb-3">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:fw[@type = 'header']" mode="linear">
        <span class="curr-title fw-bold">
            <xsl:apply-templates mode="#current"/>
            <!--Si le titre courant est précédé d'un numéro de folio avec une note-->
            <xsl:if
                test="preceding-sibling::*[1][self::tei:fw[@type = 'folio' and child::tei:note]]">
                <span class="fw-normal">
                    <xsl:call-template name="header_folioNote"/>
                </span>
            </xsl:if>
        </span>
    </xsl:template>


    <xsl:template match="tei:p" mode="diplo linear">
        <p>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:p" mode="collation">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:pc" mode="diplo linear">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:div" mode="diplo linear">
        <div>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:title" mode="diplo">
        <span class="title">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:title" mode="linear">
        <i title="Titre">
            <xsl:apply-templates mode="#current"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:choice" mode="diplo">
        <xsl:choose>
            <xsl:when test="tei:abbr or tei:expan">
                <span class="choice">
                    <span title="Abbréviation pour {tei:expan}">
                        <xsl:apply-templates select="tei:abbr" mode="#current"/>
                    </span>
                </span>
            </xsl:when>
            <xsl:when test="tei:orig or tei:reg">
                <xsl:apply-templates mode="#current"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:choice" mode="linear">
        <xsl:choose>
            <xsl:when test="tei:abbr or tei:expan">
                <span class="choice">
                    <span title="Abbrégé en {tei:abbr}">
                        <xsl:apply-templates select="tei:expan" mode="#current"/>
                    </span>
                </span>
            </xsl:when>
            <xsl:when test="tei:orig or tei:reg">
                <xsl:apply-templates mode="#current"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:choice" mode="collation">
        <xsl:choose>
            <xsl:when test="tei:abbr or tei:expan">
                <span class="choice">
                    <xsl:apply-templates select="tei:expan" mode="#current"/>
                </span>
            </xsl:when>
            <xsl:when test="tei:orig or tei:reg">
                <xsl:apply-templates mode="#current"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:orig" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:orig" mode="linear collation"/>
    <xsl:template match="tei:reg" mode="diplo"/>
    <xsl:template match="tei:reg" mode="linear collation">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:abbr" mode="diplo">
        <mark title="abbréviation">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:expan" mode="linear">
        <mark title="abbréviation">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:expan" mode="collation">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:ex" mode="linear collation">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:am" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:am" mode="linear">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:subst" mode="diplo">
        <span title="substitution">
            <span class="align-top" title="Ajout">
                <xsl:apply-templates mode="#current" select="tei:add"/>
            </span>
            <s class="align-bottom" title="Suppression">
                <xsl:apply-templates mode="#current" select="tei:del"/>
            </s>
            <xsl:if test="not(tei:add) or not(tei:del)">
                <xsl:message>Substitution étrange : add ou del seul (contexte : <xsl:value-of
                        select="parent::*"/>)</xsl:message>
            </xsl:if>
        </span>
    </xsl:template>
    <xsl:template match="tei:subst" mode="linear">
        <span title="Texte résultant d'une substitution">
            <xsl:apply-templates mode="#current" select="tei:add"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:subst" mode="collation">
        <span>
            <xsl:apply-templates mode="#current" select="tei:add"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:add" mode="#all">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:del" mode="#all">
        <s>
            <xsl:apply-templates mode="#current"/>
        </s>
    </xsl:template>

    <xsl:template match="tei:app" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:lem" mode="diplo">
        <span class="lem">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:rdg" mode="diplo"/>
    <xsl:template match="tei:rdgGrp" mode="diplo"/>

    <xsl:template match="tei:app[tei:rdg[@type = 'irrelevant'] and tei:lem[. = '']]" mode="linear"/>
    <xsl:template match="tei:app" mode="linear">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="note" as="xs:boolean" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:variable name="num">
            <xsl:number
                count='$tei//tei:div[@type = "book" and @n = $book_num]/tei:div[$chap_num]//tei:app'
                level="any"/>
        </xsl:variable>

        <span style="position: relative;">
            <xsl:for-each select="./tei:lem">
                <xsl:apply-templates select="." mode="#current"/>
            </xsl:for-each>
            <xsl:if test="not(tei:rdg[@type = 'irrelevant'])">
                <sup aria-hidden="true">
                    <a class="dropdown-toggle collation-call" href="#" id="navbarDropdown{$num}"
                        role="button" data-bs-toggle="dropdown" data-bs-auto-close="outside"
                        aria-expanded="false">
                        <span class="plus">+</span>
                    </a>
                    <span class="dropdown-menu collation-block"
                        aria-labelledby="navbarDropdown{$num}">
                        <xsl:for-each select="./node()[name() = 'rdg' or name() = 'rdgGrp']">
                            <span class="collation-content">
                                <xsl:apply-templates select="." mode="#current"/>
                            </span>
                        </xsl:for-each>
                    </span>
                </sup>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="tei:app[tei:rdg[@type = 'irrelevant']]" mode="collation"/>
    <xsl:template match="tei:app" mode="collation">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="note" as="xs:boolean" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:variable name="num">
            <xsl:number
                count='$tei//tei:div[@type = "book" and @n = $book_num]/tei:div[$chap_num]//tei:app'
                level="any"/>
        </xsl:variable>
        <div class="collation row" id="coll{$num}">
            <div class="coll-verse col-2">
                <xsl:choose>
                    <xsl:when test="ancestor::tei:l[1]">
                        <a>
                            <xsl:attribute name="href">
                                <xsl:value-of
                                    select="concat('javascript:versePointer(', ancestor::tei:l[1]/@n, ');')"
                                />
                            </xsl:attribute>
                            <xsl:text>v. </xsl:text>
                            <xsl:value-of select="ancestor::tei:l[1]/@n"/>
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <span>
                            <xsl:text>Non num.</xsl:text>
                        </span>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
            </div>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:lem[. = '']" mode="linear">
        <span class="lem-empty"/>
    </xsl:template>
    <!-- si pas de collations alors pas de mise en forme du lem -->
    <xsl:template
        match="tei:lem[not(. = '') and count(following-sibling::tei:rdg[not(@type = 'irrelevant')]) = 0]"
        mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:lem" mode="linear">
        <span class="lem">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:lem" mode="collation"/>

    <!-- collocations dans l'onglet 'collocations' -->
    <xsl:template match="tei:rdg[@type = 'irrelevant']" mode="collation"/>
    <xsl:template match="tei:rdg" mode="collation">
        <div class="coll-text col-10">
            <span>
                <span class="coll-source">
                    <xsl:value-of
                        select="replace(replace(normalize-space(@wit), '#', ''), ' ', ', ')"/>
                    <xsl:text> </xsl:text>
                </span>
                <xsl:choose>
                    <xsl:when test=".[not(@type = 'irrelevant') and . = '']">
                        <i>
                            <xsl:text>om.</xsl:text>
                        </i>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates mode="#current"/>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
        </div>
    </xsl:template>

    <xsl:template match="tei:rdg[@type = 'irrelevant']" mode="linear"/>
    <xsl:template match="tei:rdg" mode="linear">
        <xsl:choose>
            <xsl:when test=".[not(@type = 'irrelevant') and . = '']">
                <i>
                    <xsl:text>om.</xsl:text>
                </i>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates mode="collation"/>
            </xsl:otherwise>
        </xsl:choose>
        <span class="coll-source">
            <xsl:text> [</xsl:text>
            <xsl:value-of select="replace(replace(normalize-space(@wit), '#', ''), ' ', ', ')"/>
            <xsl:text>]</xsl:text>
        </span>
    </xsl:template>

    <xsl:template match="tei:rdgGrp" mode="linear">
        <xsl:for-each select="node()[name() = 'lem' or name() = 'rdg']">
            <xsl:apply-templates mode="collation"/>
            <span class="coll-source">
                <xsl:text> [</xsl:text>
                <xsl:value-of select="replace(replace(normalize-space(@wit), '#', ''), ' ', ', ')"/>
                <xsl:text>]</xsl:text>
            </span>
            <xsl:if test="not(position() = last())">
                <xsl:text> ; </xsl:text>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:rdgGrp" mode="collation">
        <div class="coll-text col-10">
            <xsl:for-each select="node()[name() = 'lem' or name() = 'rdg']">
                <span>
                    <span class="coll-source">
                        <xsl:value-of
                            select="replace(replace(normalize-space(@wit), '#', ''), ' ', ', ')"/>
                        <xsl:text> </xsl:text>
                    </span>
                    <xsl:apply-templates mode="collation"/>
                </span>
                <xsl:if test="not(position() = last())">
                    <xsl:text> ; </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="tei:ab" mode="collation">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:metamark" mode="linear diplo">
        <mark title="{@rend}">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:cit | tei:q" mode="diplo">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:cit | tei:q" mode="linear">
        <xsl:if test="not(@prev)">
            <xsl:text>« </xsl:text>
        </xsl:if>
        <xsl:apply-templates mode="#current"/>
        <xsl:if test="not(@next)">
            <xsl:text> »</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:hi[@rend = 'smallcaps']" mode="#all">
        <span style="font-variant:small-caps;">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'superscript']" mode="#all">
        <sup>
            <xsl:apply-templates mode="#current"/>
        </sup>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']" mode="#all">
        <i>
            <xsl:apply-templates mode="#current"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:lg" mode="#all">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:l" mode="diplo">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="concat('verse ', 'v', @n)"/>
            </xsl:attribute>
            <xsl:if test="@n and (xs:integer(replace(@n, '[a-z]', '')) mod 5) = 0">
                <span class="verse-number">
                    <xsl:value-of select="@n"/>
                </span>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:l" mode="linear">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="concat('verse ', 'v', @n)"/>
            </xsl:attribute>
            <xsl:if test="@n and (xs:integer(replace(@n, '[a-z]', '')) mod 5) = 0">
                <span class="verse-number">
                    <xsl:value-of select="@n"/>
                </span>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:l" mode="collation">
        <xsl:apply-templates mode="#current"/>
        <xsl:if test="count(following-sibling::tei:l) > 0">
            <xsl:text> / </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:head" mode="diplo">
        <!-- si le head est le premier élément du chapitre => on n'a pas le lien avec son image -> on le crée -->
        <xsl:if test="count(preceding-sibling::*) = 0">
            <xsl:for-each select="preceding::tei:pb[1]">
                <xsl:variable name="id" select="replace(@facs, '.*/(.*).item', '$1')"/>
                <hr data-img-id="{$id}"/>
                <xsl:for-each select="following::tei:fw[1]">
                    <p title="Numéro de {@type}" class="text-end">
                        <i>
                            <xsl:value-of select="@n"/>
                        </i>
                    </p>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:if>
        <blockquote class="ms-2">
            <xsl:apply-templates mode="#current"/>
        </blockquote>
    </xsl:template>
    <xsl:template match="tei:head" mode="linear">
        <!-- si le head est le premier élément du chapitre => on n'a pas le lien avec son image -> on le crée -->
        <xsl:if test="count(preceding-sibling::*) = 0">
            <xsl:for-each select="preceding::tei:pb[1]">
                <xsl:variable name="id" select="replace(@facs, '.*/(.*).item', '$1')"/>
                <hr data-img-id="{$id}"/>
            </xsl:for-each>
        </xsl:if>
        <!-- plus nécessaire vu qu'on a changé l'affichage des collations -->
        <!--<xsl:choose>
            <xsl:when test=".//tei:app[count(.//tei:rdg[not(@type = 'irrelevant')])]">
                <xsl:variable name="max_count_rdg"
                    select="max(.//tei:app/count(.//tei:rdg[not(@type = 'irrelevant')]))"/>
                <xsl:variable name="count_l" select="count(.//tei:l)"/>
                <blockquote class="ms-2"
                    style="vertical-align: top; height:{24+(($max_count_rdg+$count_l)*24)}px;">
                    <xsl:apply-templates mode="#current"/>
                </blockquote>
            </xsl:when>
            <xsl:otherwise>
                <blockquote class="ms-2">
                    <xsl:apply-templates mode="#current"/>
                </blockquote>
            </xsl:otherwise>
        </xsl:choose>-->
        <!-- ajout suite a suppression précédente -->
        <blockquote class="ms-2">
            <xsl:apply-templates mode="#current"/>
        </blockquote>
    </xsl:template>

    <xsl:template match="tei:l[.//tei:app[count(.//tei:rdg[not(@type = 'irrelevant')]) > 0]]"
        mode="linear">
        <xsl:variable name="max_count_rdg"
            select="max(.//tei:app/count(.//tei:rdg[not(@type = 'irrelevant')]))"/>
        <!--        <span style="height:{24+($max_count_rdg*24)}px;">-->
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="concat('verse ', 'v', @n)"/>
            </xsl:attribute>
            <xsl:if test="@n and (xs:integer(replace(@n, '[a-z]', '')) mod 5) = 0">
                <span class="verse-number">
                    <xsl:value-of select="@n"/>
                </span>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>


    <xsl:template match="tei:said" mode="diplo">
        <span title="Discours direct">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:said" mode="linear">
        <span title="Discours direct">
            <xsl:text>« </xsl:text>
            <xsl:apply-templates mode="#current"/>
            <xsl:text> »</xsl:text>
        </span>
    </xsl:template>

    <xsl:template match="tei:foreign" mode="#all">
        <mark title="Emprunt">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:anchor" mode="#all">
        <a id="{@xml:id}">
            <i class="fas fa-anchor" id="{@xml:id}"/>
        </a>
    </xsl:template>

    <xsl:template match="tei:lb" mode="diplo">
        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
    </xsl:template>
    <xsl:template match="tei:lb" mode="linear collation">
        <xsl:if test="not(@break = 'no')">
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:note" mode="diplo"/>
    <xsl:template match="tei:note" mode="linear">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="note" as="xs:boolean" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:if test="name(parent::*) != 'fw'">
            <xsl:variable name="num">
                <xsl:number
                    count='$tei//tei:div[@type = "book" and @n = $book_num]/tei:div[$chap_num]//tei:note'
                    level="any"/>
            </xsl:variable>
            <sup class="modal-call note-call" aria-hidden="true" id="noteCall{$num}"
                data-modal-id="noteModal{$num}">
                <!--Add css change cursor -->
                <xsl:value-of select="$num"/>
            </sup>
        </xsl:if>
    </xsl:template>

    <xsl:template name="header_folioNote">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="note" as="xs:boolean" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <!--Affichage de la note de folio à la fin du titre en manchette-->
        <xsl:if test="preceding-sibling::*[1][self::tei:fw[@type = 'folio']]">
            <xsl:for-each
                select="$tei//tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]//tei:note">
                <xsl:variable name="num">
                    <xsl:number value="position()" format="1"/>
                </xsl:variable>
                <sup class="modal-call note-call" aria-hidden="true" id="noteCall{$num}"
                    data-modal-id="noteModal{$num}">
                    <!--Add css change cursor -->
                    <xsl:value-of select="$num"/>
                </sup>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:note" mode="note">
        <xsl:param name="tei" required="yes" tunnel="yes"/>
        <xsl:param name="book_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:param name="chap_num" required="yes" tunnel="yes" as="xs:integer"/>
        <xsl:variable name="num">
            <xsl:number
                count="$tei//tei:div[@type = 'book' and @n = $book_num]/tei:div[$chap_num]//tei:note"
                level="any"/>
        </xsl:variable>
        <h4 class="modal-title" modal-title="noteModal{$num}">Note n°<xsl:value-of select="$num"
            /></h4>
        <div class="modal-text" modal-text="noteModal{$num}">
            <xsl:apply-templates mode="linear"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:persName" mode="diplo collation">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>
    <xsl:template match="tei:persName" mode="linear">
        <xsl:variable name="listPerson" select="//tei:person"/>
        <mark class="persName">
            <xsl:apply-templates mode="#current"/>
            <span>
                <xsl:attribute name="class">
                    <!--Afficher la note à gauche plutôt qu'à droite si elle est sur un titre courant
                        (pour éviter qu'elle ne soit coupée)-->
                    <xsl:choose>
                        <xsl:when test="ancestor::tei:fw[1 and @type = 'header']">
                            <xsl:text>persName-info fw-normal on-left</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>persName-info fw-normal on-right</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:if test="count(tokenize(@ref, ' ')) > 1">
                    <i class="d-block text-center">Ambiguïté</i>
                </xsl:if>
                <xsl:for-each select="tokenize(@ref, ' ')">
                    <xsl:variable name="id" select="replace(., '#', '')"/>
                    <xsl:variable name="person" select="$listPerson[@xml:id = $id]"/>
                    <xsl:if test="not(count($listPerson[@xml:id = $id]) = 1)">
                        <xsl:message>
                            <xsl:value-of
                                select="concat('persName: ', ., ' [', $id, '] ', count($listPerson[@xml:id = $id]))"
                            />
                        </xsl:message>
                    </xsl:if>
                    <b>
                        <xsl:value-of select="$person/tei:persName[@type = 'normalise']"/>
                    </b>
                    <xsl:if test="$person/tei:birth or $person/tei:death">
                        <xsl:text> (</xsl:text>
                        <xsl:call-template name="get_date">
                            <xsl:with-param name="elem" as="node()">
                                <xsl:copy-of select="$person/tei:birth"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:text> — </xsl:text>
                        <xsl:call-template name="get_date">
                            <xsl:with-param name="elem" as="node()">
                                <xsl:copy-of select="$person/tei:death"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                    <xsl:if test="$person/tei:occupation">
                        <!-- simulate an <hr/> -->
                        <span class="hr-100">
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </span>
                        <xsl:for-each select="$person/tei:occupation">
                            <!-- todo: notAfter notBefore) -->
                            <xsl:value-of select="."/>
                            <xsl:text> (</xsl:text>
                            <xsl:call-template name="format_date">
                                <xsl:with-param name="val" select="@from"/>
                            </xsl:call-template>
                            <xsl:text>-</xsl:text>
                            <xsl:call-template name="format_date">
                                <xsl:with-param name="val" select="@to"/>
                            </xsl:call-template>
                            <xsl:text>)</xsl:text>
                            <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="$person/tei:note and not($person/tei:note = '')">
                        <!-- simulate an <hr/> -->
                        <span class="hr-short">
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </span>
                        <xsl:value-of select="$person/tei:note"/>
                    </xsl:if>
                    <xsl:if test="not(position() = last())">
                        <b class="d-block text-center">ou bien</b>
                    </xsl:if>
                </xsl:for-each>
            </span>
        </mark>
    </xsl:template>

    <xsl:template name="get_date">
        <!-- expect <tei:birth/> like element -->
        <xsl:param name="elem"/>
        <xsl:choose>
            <xsl:when test="not($elem = '')">
                <xsl:value-of select="$elem/text()"/>
            </xsl:when>
            <xsl:when test="$elem/@when">
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@when"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$elem/@notBefore and $elem/@notAfter">
                <xsl:text>entre </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notBefore"/>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:text> et </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notAfter"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$elem/@notBefore and not($elem/@notAfter)">
                <xsl:text>après </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notBefore"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="not($elem/@notBefore) and $elem/@notAfter">
                <xsl:text>avant </xsl:text>
                <xsl:call-template name="format_date">
                    <xsl:with-param name="val">
                        <xsl:value-of select="$elem/@notAfter"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="format_date">
        <xsl:param name="val"/>
        <xsl:choose>
            <xsl:when test="string-length($val) = 4">
                <xsl:value-of select="xs:integer($val)"/>
            </xsl:when>
            <xsl:when test="string-length($val) = 5 and starts-with($val, '-')">
                <xsl:value-of select="xs:integer(substring($val, 2))"/>
                <xsl:text> av&non_breakable_space;J.C.</xsl:text>
            </xsl:when>
            <xsl:when test="string-length($val) = 10">
                <xsl:value-of select="substring($val, 9, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="substring($val, 6, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="xs:integer(substring($val, 1, 4))"/>
            </xsl:when>
            <xsl:when test="string-length($val) = 11 and starts-with($val, '-')">
                <xsl:value-of select="substring($val, 10, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="substring($val, 7, 2)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="xs:integer(substring($val, 2, 4))"/>
                <xsl:text> av&non_breakable_space;J.C.</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>?</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- À AFFINER -->
    <xsl:template
        match="tei:gap | tei:num | tei:unclear | tei:date | tei:addSpan | tei:damage | tei:quote | tei:time | tei:floatingText | tei:body"
        mode="#all">
        <span class="{name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:geogName | tei:orgName | tei:placeName" mode="diplo linear">
        <mark title="{name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:geogName | tei:orgName | tei:placeName" mode="collation">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="text()" mode="diplo">
        <xsl:variable name="s">
            <xsl:choose>
                <!-- S doit rester S en début de vers -->
                <!-- pas trouvé de tei:c -->
                <xsl:when test="not(parent::tei:c) and not(parent::tei:orig) and not(parent::tei:reg)">
                    <!--<xsl:value-of select="replace(replace(replace(replace(., 's', 'ſ'), 'ſ\b', 's', '!'), 'S’', 'ſ’'), 's’', 'ſ’')"/>-->
                    <xsl:value-of select="replace(replace(replace(., 's', 'ſ'), 'ſ\b', 's', '!'), 's’', 'ſ’')"/>
                    <!--si premier mot-->
                    <!--<xsl:if test="position() = 1">
                        |||<xsl:value-of select="$c"/>|||
                    </xsl:if>-->
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="c">
            <xsl:value-of select="replace($s, 'ç', 'c')"/>
        </xsl:variable>
        <xsl:variable name="j_min">
            <xsl:value-of select="replace($c, 'j', 'i')"/>
        </xsl:variable>
        <xsl:variable name="j_maj">
            <xsl:value-of select="replace($j_min, 'J', 'I')"/>
        </xsl:variable>
        <xsl:variable name="r">
            <xsl:choose>
                <xsl:when
                    test="not(ancestor::tei:head[@type = 'introVerse'] or ancestor::tei:fw[@type = 'header'] or ancestor::tei:floatingText)">
                    <xsl:value-of select="replace($j_maj, '([BbDdGgOoPphy])r', '$1ꝛ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$j_maj"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="v">
            <xsl:choose>
                <!--u devient v en début de mot sauf s'il y a un 'n' derrière-->
                <!--à vérifier ce que faisait la version d'avant pour être sûr de ne pas créer un autre problème-->
                <!--<xsl:when test="not(preceding-sibling::*[1]/name() = 'choice')">-->
                <xsl:when test="not(parent::tei:choice)">
                    <xsl:value-of select="replace(replace($r, 'v', 'u'), '\bu([^n])', 'v$1', '!')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="replace($r, 'v', 'u')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="apostrophe">
            <xsl:variable name="quote">'’’</xsl:variable>
            <xsl:value-of select="replace($v, concat('[', $quote, ']'), '')"/>
        </xsl:variable>
        <xsl:variable name="accent">
            <xsl:variable name="accents">éèëÀàäùüïÿö</xsl:variable>
            <xsl:variable name="vowel">eeeAaauuiyo</xsl:variable>
            <xsl:value-of select="translate($apostrophe, $accents, $vowel)"/>
        </xsl:variable>
        <xsl:value-of select="$accent"/>
    </xsl:template>

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*" mode="#all">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
        <xsl:message>
            <xsl:text>Élement non pris en charge par la XSLT : </xsl:text>
            <xsl:value-of select="name(.)"/>
            <!--<xsl:copy/>
            <xsl:copy-of select="@*"/>-->
        </xsl:message>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
