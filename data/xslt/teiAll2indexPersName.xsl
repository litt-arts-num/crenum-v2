<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation for creating in index of persName using standoff/person informations
* @author AnneGF@CNRS
* @date : 2022-2023
*/
--><!DOCTYPE tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:param name="show_page_ref" as="xs:boolean">0</xsl:param>
    <!--
        $show_page_ref param
        If 0, don't show page ref for verse parts ;
        If 1, group verse refence by page and show it ;
        Do not change anything for the tei:div[@type = 'prologueProse'] part
    -->
    <!-- Not implemented yet. Will see if useful...
    <xsl:param name="show_all_person" as="xs:boolean">0</xsl:param> -->
    <!--
        $show_all_person
        If 0, don't show non referenced person (standOff/person exists but no persName with @ref corresponding to its @xml:id) ;
        If 1, show all declared standOff/person ;
    -->

    <xsl:template match="/">
        <!-- Alphabetical navigation within the page -->
        <nav class="text-center mb-3">
            <!-- Show only letter "used" (with at least one persName initiating with this letter) -->
            <xsl:for-each-group
                select="//tei:standOff[@xml:id = 'crenum_teiheader_liste_personnes']//tei:person[count(@xml:id = //tei:persName/@ref/replace(., '#', '')) > 0]/tei:persName"
                group-by="substring(normalize-space(.), 1, 1)">
                <xsl:sort select="current-grouping-key()" lang="fr"/>
                <!-- Remove some accentuated or specific letter -->
                <xsl:if test="not(matches(current-grouping-key(), '[ÆÉa-z]'))">
                    <a class="goto" href="#alpha_{current-grouping-key()}">
                        <xsl:value-of select="current-grouping-key()"/>
                    </a>
                    <xsl:if test="not(position() = last())">
                        <xsl:text> - </xsl:text>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each-group>
        </nav>

        <div>
            <xsl:for-each-group
                select="//tei:standOff[@xml:id = 'crenum_teiheader_liste_personnes']//tei:person[count(@xml:id = //tei:persName/@ref/replace(., '#', '')) > 0]/tei:persName"
                group-by="substring(normalize-space(.), 1, 1)">
                <xsl:sort select="current-grouping-key()" lang="fr"/>
                <xsl:if test="not(matches(current-grouping-key(), '[ÆÉa-z]'))">
                    <h4 id="alpha_{current-grouping-key()}" class="text-center mt-3">
                        <xsl:value-of select="current-grouping-key()"/>
                    </h4>
                </xsl:if>
                <xsl:for-each select="current-group()">
                    <xsl:sort select="normalize-space(.)" lang="fr"/>
                    <xsl:variable name="id" select="parent::tei:person/@xml:id"/>
                    <xsl:if test="not(. = preceding-sibling::tei:persName)">
                        <xsl:choose>
                            <xsl:when test="@type = 'normalise'">
                                <h5 id="{$id}" class="mt-3">
                                    <xsl:value-of select="."/>
                                </h5>
                                <xsl:for-each
                                    select="//tei:div[@type = 'book' and count(.//tei:persName[replace(@ref, '#', '') = $id]) > 0]">
                                    <xsl:variable name="book" select="@n"/>
                                    <xsl:text>Livre </xsl:text>
                                    <xsl:value-of select="$book"/>
                                    <xsl:text> : </xsl:text>
                                    <xsl:variable name="persName_nb_tot" select="count(//tei:div[@type = 'book' and @n = $book]//tei:l//tei:persName[replace(@ref, '#', '') = $id])"/>
                                    <span>
                                        <xsl:for-each select="./tei:div">
                                            <xsl:variable name="chap_num" as="xs:integer">
                                                <xsl:number select="." count="tei:div" level="single"/>
                                            </xsl:variable>
                                            <xsl:variable name="chap">
                                                <xsl:value-of select="$chap_num"/>
                                                <xsl:text>_</xsl:text>
                                                <xsl:value-of select="@type"/>
                                                <xsl:if test="@n">
                                                    <xsl:text>_</xsl:text>
                                                    <xsl:value-of select="@n"/>
                                                </xsl:if>
                                            </xsl:variable>
                                            <xsl:if test="@type = 'prologueProse'">
                                                <xsl:for-each
                                                    select="//tei:div[@type = 'prologueProse']//tei:persName[replace(@ref, '#', '') = $id]">
                                                    <!--p{replace(replace(preceding::tei:pb[1]/@facs,'.*/f',''),'.item','')}">-->
                                                    <a
                                                        href="/book{$book}/{$chap}/p{replace(replace(preceding::tei:pb[1]/@facs,'.*/f',''),'.item','')}">
                                                        <xsl:value-of select="preceding::tei:fw[1]/@n"/>
                                                    </a>
                                                    <xsl:if test="(position() &lt; last() and count(//tei:div[@type = 'prologueProse']//tei:persName[replace(@ref, '#', '') = $id]) > 0) or (count(//tei:div[@type = 'prologueProse']//tei:persName[replace(@ref, '#', '') = $id]) > 0 and count(//tei:l//tei:persName[replace(@ref, '#', '') = $id]) > 0)">, </xsl:if>
                                                </xsl:for-each>
                                            </xsl:if>
                                            <xsl:for-each-group
                                                select=".//tei:l//tei:persName[replace(@ref, '#', '') = $id]"
                                                group-by="preceding::tei:fw[@type = 'folio'][1]/@n">
                                                <xsl:if test="$show_page_ref">
                                                    <!--p{replace(replace(preceding::tei:pb[1]/@facs,'.*/f',''),'.item','')}">-->
                                                    <a
                                                        href="/book{$book}/{$chap}/p{replace(replace(preceding::tei:pb[1]/@facs,'.*/f',''),'.item','')}">
                                                      <xsl:value-of
                                                      select="preceding::tei:fw[@type = 'folio'][1]/@n"
                                                      />
                                                    </a>
                                                    <xsl:text> [</xsl:text>
                                                </xsl:if>
                                                <xsl:for-each-group select="current-group()"
                                                    group-by="ancestor::tei:l/@n">
                                                    <!--p{replace(replace(preceding::tei:pb[1]/@facs,'.*/f',''),'.item','')}#{ancestor::tei:l/@n}">-->
                                                    <xsl:variable name="prev_persName_nb" select="count(preceding::tei:l[ancestor::tei:div[@type='book']/@n=$book]//tei:persName[replace(@ref, '#', '') = $id])"/>
                                                    <a
                                                        href="/book{$book}/{$chap}/p{replace(replace(preceding::tei:pb[1]/@facs,'.*/f',''),'.item','')}#{ancestor::tei:l/@n}">
                                                        <xsl:value-of select="ancestor::tei:l/@n"/>
                                                    </a>
                                                    <xsl:if test="($prev_persName_nb + 1) &lt; $persName_nb_tot">, </xsl:if>
                                                </xsl:for-each-group>
                                                <xsl:if test="$show_page_ref">
                                                    <xsl:text>]</xsl:text>
                                                </xsl:if>
                                            </xsl:for-each-group>
                                        </xsl:for-each>   
                                    </span>
                                    <xsl:if test="not(position() = last())">
                                        <xsl:text disable-output-escaping="yes">&lt;br/></xsl:text>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                                <h5 class="mt-3">
                                    <xsl:variable name="name" select="."/>
                                    <xsl:attribute name="id" select="concat($id,'_',$name)"/>
                                    <xsl:value-of select="."/>
                                </h5>
                                <i>
                                    <xsl:text>Voir </xsl:text>
                                    <a class="goto" href="#{$id}">
                                        <xsl:value-of
                                            select="preceding-sibling::tei:persName[@type = 'normalise']"
                                        />
                                    </a>
                                </i>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:for-each>
            </xsl:for-each-group>
        </div>
    </xsl:template>

</xsl:stylesheet>
