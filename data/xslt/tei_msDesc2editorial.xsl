<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation of editorial pages from TEI to HTML, initialy designed for CorrProust Project
* @author AnneGF@CNRS
* @date : 2022-2023
*/
-->

<!DOCTYPE tei2editorial [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/">
        <div class="col">
            <nav class="text-center mb-3 small">
                <xsl:text>Aller&non_breakable_space;à&non_breakable_space;:</xsl:text>
                <xsl:for-each
                    select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/@xml:id">
                    <a class="goto m-2" href="#{.}">
                        <xsl:value-of select="."/>
                    </a>
                </xsl:for-each>
            </nav>
            <xsl:apply-templates
                select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:msDesc">
        <h4 id="{@xml:id}">
            <xsl:value-of select="tei:msIdentifier/tei:settlement"/>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="tei:msIdentifier/tei:repository"/>
            <xsl:if test="tei:msIdentifier/tei:idno">
                <xsl:for-each select="tei:msIdentifier/tei:idno">
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="."/>
                </xsl:for-each>
            </xsl:if>
            <code>
                <xsl:text> [</xsl:text>
                <i>
                    <xsl:value-of select="@xml:id"/>
                </i>
                <xsl:text>]</xsl:text>
            </code>
        </h4>


        <h5>Contenus</h5>
        <div class="subsection">
            <xsl:apply-templates select="tei:msContents"/>
        </div>
        <h5>Supports et composition</h5>
        <div class="subsection">
            <xsl:apply-templates select="tei:physDesc/tei:objectDesc/tei:supportDesc"/>
        </div>
        <h5>Dimensions et préparation de la page</h5>
        <div class="subsection">
            <xsl:apply-templates select="tei:physDesc/tei:objectDesc"/>
        </div>

        <xsl:if test="tei:physDesc/tei:handDesc">
            <xsl:if test="normalize-space(tei:physDesc/tei:handDes)">
                <h5>Écriture</h5>
                <div class="subsection">
                    <xsl:apply-templates select="tei:msContents"/>
                </div>
            </xsl:if>
        </xsl:if>
        <xsl:if test="normalize-space(tei:physDesc/tei:objectDesc/tei:supportDesc)">
            <h5>Supports et composition</h5>
            <div class="subsection">
                <xsl:apply-templates select="tei:physDesc/tei:objectDesc/tei:supportDesc"/>
            </div>
        </xsl:if>
        <xsl:if test="normalize-space(tei:physDesc/tei:objectDesc)">
            <h5>Dimensions et préparation de la page</h5>
            <div class="subsection">
                <xsl:apply-templates select="tei:physDesc/tei:objectDesc"/>
            </div>
        </xsl:if>
        <xsl:if test="tei:physDesc/tei:handDesc">
            <xsl:if test="normalize-space(tei:physDesc/tei:handDesc)">
                <h5>Écriture</h5>
                <div class="subsection">
                    <xsl:apply-templates select="tei:physDesc/tei:handDesc"/>
                </div>
            </xsl:if>
        </xsl:if>
        <xsl:if test="tei:physDesc/tei:decoDesc">
            <xsl:if test="normalize-space(tei:physDesc/tei:decoDesc)">
                <h5>Mise en page et décoration</h5>
                <div class="subsection">
                    <xsl:apply-templates select="tei:physDesc/tei:decoDesc"/>
                </div>
            </xsl:if>
        </xsl:if>
        <xsl:if test="tei:history/tei:origin/tei:origDate">
            <xsl:if test="normalize-space(tei:history/tei:origin/tei:origDate)">
                <h5>HOrigine du manuscrit</h5>
                <div class="subsection">
                    <xsl:apply-templates select="tei:history/tei:origin/tei:origDate"/>
                </div>
            </xsl:if>
        </xsl:if>

        <xsl:if test="tei:history/tei:provenance">
            <xsl:for-each select="tei:history/tei:provenance">
                <xsl:if test="normalize-space(.)">
                    <h5>Histoire et traces d’usage</h5>
                    <div class="subsection">
                        <xsl:apply-templates select="."/>
                    </div>
                </xsl:if>
            </xsl:for-each>
        </xsl:if>

        <xsl:if test="tei:additional/tei:listBibl">
            <xsl:if test="normalize-space(tei:additional/tei:listBibl)">
                <h5>Bibliographie</h5>
                <div class="subsection">
                    <p>
                        <xsl:value-of select="tei:additional/tei:listBibl"/>
                    </p>
                </div>
            </xsl:if>
        </xsl:if>

        <xsl:if test="tei:additional/tei:surrogates/tei:bibl/tei:ref">
            <xsl:if test="normalize-space(tei:additional/tei:surrogates/tei:bibl/tei:ref)">
                <h5>Numérisation</h5>
                <div class="subsection">
                    <a href="{tei:additional/tei:surrogates/tei:bibl/tei:ref/@target}"
                        target="_blank">
                        <xsl:value-of
                            select="tei:additional/tei:surrogates/tei:bibl/tei:ref/@target"/>
                    </a>
                </div>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <!-- Contenus -->
    <xsl:template match="tei:msContents">
        <ul>
            <xsl:for-each select="tei:msItem">
                <li>
                    <xsl:choose>
                        <xsl:when test="tei:p and count(child::*) = 1">
                            <xsl:apply-templates select="tei:p"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="tei:locus">
                                <xsl:value-of select="tei:locus"/>
                                <xsl:text>&non_breakable_space;: </xsl:text>
                            </xsl:if>
                            <xsl:if test="tei:title">
                                <xsl:value-of select="tei:title"/>
                            </xsl:if>
                            <!--<xsl:if test="tei:author">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="tei:author"/>
                    </xsl:if>-->
                            <xsl:text>.</xsl:text>
                            <xsl:if test="tei:rubric">
                                <xsl:text> </xsl:text>
                                <i>
                                    <xsl:apply-templates select="tei:rubric"/>
                                </i>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <!-- Support et composition -->
    <xsl:template match="tei:supportDesc">
        <p>
            <xsl:value-of select="tei:support"/>
            <xsl:text>. </xsl:text>
            <xsl:value-of select="tei:extent/text()"/>
        </p>
    </xsl:template>

    <!-- Dimensions et préparation de la page -->
    <xsl:template match="tei:physDesc/tei:objectDesc">
        <p>
            <xsl:value-of select="tei:supportDesc/tei:extent/tei:dimensions/tei:height"/>
            <xsl:text> x </xsl:text>
            <xsl:value-of select="tei:supportDesc/tei:extent/tei:dimensions/tei:width"/>
            <xsl:text> mm. </xsl:text>
            <xsl:value-of select="tei:layoutDesc/tei:layout/@columns"/>
            <xsl:text> colonne</xsl:text>
            <xsl:if test="xs:integer(tei:layoutDesc/tei:layout/@columns) > 1">
                <xsl:text>s</xsl:text>
            </xsl:if>
            <xsl:text>, </xsl:text>
            <xsl:choose>
                <xsl:when test="contains(tei:layoutDesc/tei:layout/@writtenLines, ' ')">
                    <xsl:value-of
                        select="replace(tei:layoutDesc/tei:layout/@writtenLines, ' ', ' à ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="tei:layoutDesc/tei:layout/@writtenLines"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:text> ligne</xsl:text>
            <xsl:if
                test="xs:integer(tokenize(tei:layoutDesc/tei:layout/@writtenLines, ' ')[last()]) > 1">
                <xsl:text>s</xsl:text>
            </xsl:if>
            <xsl:text> à la page.</xsl:text>
            <xsl:if test="tei:layoutDesc/tei:layout/tei:p">
                <xsl:text> </xsl:text>
                <xsl:apply-templates select="tei:layoutDesc/tei:layout/tei:p"/>
            </xsl:if>
        </p>
    </xsl:template>

    <!-- Écriture-->
    <xsl:template match="tei:physDesc/tei:handDesc">
        <p>
            <xsl:apply-templates select="tei:p"/>
        </p>
    </xsl:template>

    <!-- Mise en page et décoration -->
    <xsl:template match="tei:physDesc/tei:decoDesc">
        <p>
            <xsl:apply-templates select="tei:p"/>
        </p>
    </xsl:template>

    <!-- Date de création du manuscrit -->
    <xsl:template match="tei:history/tei:oroigin/tei:origDate">
        <p>
            <xsl:apply-templates select="text()"/>
        </p>
    </xsl:template>

    <!-- Histoire et traces d’usage -->
    <xsl:template match="tei:history/tei:provenance">
        <p>
            <xsl:apply-templates select="tei:p | text()"/>
        </p>
    </xsl:template>

    <xsl:template match="tei:p">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:rubric">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*">
        <b style="color:red">
            <xsl:value-of select="name(.)"/>
        </b>
    </xsl:template>
</xsl:stylesheet>
