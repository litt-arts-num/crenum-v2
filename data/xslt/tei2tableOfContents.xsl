<?xml version="1.0" encoding="UTF-8"?><!--
/**

* XSLT for transformation of TEI to HTML : table of contents
* @author RachelGaubil@UGA
* @date : 2023
*/
-->

<!DOCTYPE tei2tableOfContents [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">
    <xsl:import href="tei2chapter.xsl"/>
    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!--Enlève les espaces superflus-->
    <xsl:strip-space elements="*"/>

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">

        <xsl:variable name="output1">
            <xsl:text>../../templates/htm/chapters_list.htm</xsl:text>
        </xsl:variable>

        <xsl:result-document method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"
            href="{$output1}">

            <xsl:for-each select="tei:TEI/tei:text/tei:body//tei:div[@type = 'book']">
                <xsl:variable name="book" select="@n"/>

                <xsl:text>{% macro book</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>() %}</xsl:text>
                <div class="col-8 text-center">

                    <xsl:for-each select="./tei:div">
                        <xsl:variable name="chap" as="xs:integer">
                            <xsl:value-of select="position()"/>
                        </xsl:variable>
                        <xsl:variable name="chap_link">
                            <xsl:text>/book</xsl:text>
                            <xsl:value-of select="$book"/>
                            <xsl:text>/</xsl:text>
                            <xsl:value-of select="$chap"/>
                            <xsl:text>_</xsl:text>
                            <xsl:value-of select="@type"/>
                            <xsl:if test="@n">
                                <xsl:text>_</xsl:text>
                                <xsl:value-of select="@n"/>
                            </xsl:if>
                        </xsl:variable>

                        <xsl:call-template name="generate_chapters_list">
                            <xsl:with-param name="link" select="$chap_link"/>
                        </xsl:call-template>
                    </xsl:for-each>
                </div>
                <xsl:text>
                    {% endmacro %}
                </xsl:text>
            </xsl:for-each>
        </xsl:result-document>



        <xsl:for-each select="tei:TEI/tei:text/tei:body//tei:div[@type = 'book']">
            <xsl:sort select="@n" data-type="number"/>
            <xsl:variable name="book" select="@n"/>
            <xsl:variable name="output2">
                <xsl:text>../../templates/htm/table_of_contents_</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>.htm</xsl:text>
            </xsl:variable>


            <xsl:result-document method="html" indent="yes" encoding="UTF-8"
                omit-xml-declaration="yes" href="{$output2}">

                <div class="accordion accordion-flush" id="{concat('accordionFlushExample',$book)}">
                    <xsl:for-each select="./tei:div">
                        <xsl:variable name="chap" as="xs:integer">
                            <xsl:value-of select="position()"/>
                        </xsl:variable>
                        <xsl:variable name="chap_link">
                            <xsl:text>/book</xsl:text>
                            <xsl:value-of select="$book"/>
                            <xsl:text>/</xsl:text>
                            <xsl:value-of select="$chap"/>
                            <xsl:text>_</xsl:text>
                            <xsl:value-of select="@type"/>
                            <xsl:if test="@n">
                                <xsl:text>_</xsl:text>
                                <xsl:value-of select="@n"/>
                            </xsl:if>
                        </xsl:variable>


                        <div class="accordion-item">
                            <xsl:call-template name="generate_chapters_tableOfCont">
                                <xsl:with-param name="book" select="$book"/>
                                <xsl:with-param name="chap" select="$chap"/>
                                <xsl:with-param name="link" select="$chap_link"/>
                            </xsl:call-template>
                            
                            <xsl:choose>
                                
                                <!-- Prologues en vers sans titre courant -->
                                <xsl:when
                                    test="@type = 'prologueVerse' and not(.//tei:fw[@type = 'header'])">
                                    <xsl:variable name="title_link">
                                        <xsl:value-of select="$chap_link"/>
                                        <xsl:text>/p</xsl:text>
                                        <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                        <xsl:value-of
                                            select="substring-before(substring-after(following::tei:pb[1]/@facs, '/f'), '.item')"
                                        />
                                    </xsl:variable>
                                    <xsl:call-template name="generate_titles_tableOfCont">
                                        <xsl:with-param name="value"><xsl:text>Prologue en vers</xsl:text></xsl:with-param>
                                        <xsl:with-param name="title"><xsl:text>false</xsl:text></xsl:with-param>
                                        <xsl:with-param name="book" select="$book"/>
                                        <xsl:with-param name="chap" select="$chap"/>
                                        <xsl:with-param name="link" select="$title_link"/>
                                    </xsl:call-template>
                                </xsl:when>
                                
                                <!-- Prologues en prose sans titre courant -->
                                <xsl:when
                                    test="@type = 'prologueProse' and not(.//tei:fw[@type = 'header'])">
                                    <xsl:variable name="title_link">
                                        <xsl:value-of select="$chap_link"/>
                                        <xsl:text>/p</xsl:text>
                                        <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                        <xsl:value-of
                                            select="substring-before(substring-after(following::tei:pb[1]/@facs, '/f'), '.item')"
                                        />
                                    </xsl:variable>
                                    <xsl:call-template name="generate_titles_tableOfCont">
                                        <xsl:with-param name="value"><xsl:text>Prologue en prose</xsl:text></xsl:with-param>
                                        <xsl:with-param name="title"><xsl:text>false</xsl:text></xsl:with-param>
                                        <xsl:with-param name="book" select="$book"/>
                                        <xsl:with-param name="chap" select="$chap"/>
                                        <xsl:with-param name="link" select="$title_link"/>
                                    </xsl:call-template>
                                </xsl:when>
                                
                                <xsl:otherwise>
                                    
                                    <!-- Cas des chapitres en vers sans titre courant -> pas besoin si on parcours les folios ?-->
                                    <xsl:if test=".//tei:head[@type = 'introVerse'] and .//tei:head[@type = 'introVerse' and not(preceding-sibling::tei:fw[@type = 'header'])]">
                                        <xsl:for-each
                                            select=".//tei:head[@type = 'introVerse']//tei:l[1]">
                                            <xsl:variable name="title_link">
                                                <xsl:value-of select="$chap_link"/>
                                                <xsl:text>/p</xsl:text>
                                                <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                <xsl:value-of
                                                    select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                />
                                            </xsl:variable>
                                            <xsl:call-template name="generate_titles_tableOfCont">
                                                <xsl:with-param name="value"><xsl:text></xsl:text></xsl:with-param>
                                                <xsl:with-param name="title"><xsl:text>false</xsl:text></xsl:with-param>
                                                <xsl:with-param name="book" select="$book"/>
                                                <xsl:with-param name="chap" select="$chap"/>
                                                <xsl:with-param name="link" select="$title_link"/>
                                            </xsl:call-template>
                                        </xsl:for-each>
                                    </xsl:if>
                                    
                                    <xsl:for-each select=".//tei:fw[@type = 'folio']">
                                        <span class="ok"/>
                                        <xsl:choose>
                                            
                                            <!-- empty folio -->
                                            <xsl:when test="following-sibling::*[1][self::tei:div] or following-sibling::*[1][self::tei:pb]">
                                                <xsl:variable name="title_link">
                                                    <xsl:value-of select="$chap_link"/>
                                                    <xsl:text>/p</xsl:text>
                                                    <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                    <xsl:value-of
                                                        select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                    />
                                                </xsl:variable>
                                                <xsl:call-template name="generate_titles_tableOfCont">
                                                    <xsl:with-param name="value"><xsl:text>miniature</xsl:text></xsl:with-param>
                                                    <xsl:with-param name="title"><xsl:text>false</xsl:text></xsl:with-param>
                                                    <xsl:with-param name="book" select="$book"/>
                                                    <xsl:with-param name="chap" select="$chap"/>
                                                    <xsl:with-param name="link" select="$title_link"/>
                                                </xsl:call-template>
                                            </xsl:when>
                                            
                                            <!-- Chapitres avec titres courants -->
                                            <xsl:when test="following-sibling::tei:fw[1][@type = 'header']">
                                                <xsl:for-each select="following-sibling::tei:fw[1][@type = 'header']">
                                                    <xsl:variable name="title_link">
                                                        <xsl:value-of select="$chap_link"/>
                                                        <xsl:text>/p</xsl:text>
                                                        <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                        <xsl:value-of
                                                            select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                        />
                                                    </xsl:variable>
                                                    <xsl:call-template name="generate_titles_tableOfCont">
                                                        <xsl:with-param name="value"><xsl:text></xsl:text></xsl:with-param>
                                                        <xsl:with-param name="title"><xsl:text>true</xsl:text></xsl:with-param>
                                                        <xsl:with-param name="book" select="$book"/>
                                                        <xsl:with-param name="chap" select="$chap"/>
                                                        <xsl:with-param name="link" select="$title_link"/>
                                                    </xsl:call-template>
                                                </xsl:for-each>
                                            </xsl:when>
                                            
                                            <!-- Chapitres sans titres courants -->
                                            <xsl:otherwise>
                                                <xsl:variable name="title_link">
                                                    <xsl:value-of select="$chap_link"/>
                                                    <xsl:text>/p</xsl:text>
                                                    <!-- récupération du numéro de page depuis l'url gallica dans le pb suivant car on est au niveau de la div de chapitre -->
                                                    <xsl:value-of
                                                        select="substring-before(substring-after(preceding::tei:pb[1]/@facs, '/f'), '.item')"
                                                    />
                                                </xsl:variable>
                                                <xsl:call-template name="generate_titles_tableOfCont">
                                                    <xsl:with-param name="value"><xsl:text></xsl:text></xsl:with-param>
                                                    <xsl:with-param name="title"><xsl:text>false</xsl:text></xsl:with-param>
                                                    <xsl:with-param name="book" select="$book"/>
                                                    <xsl:with-param name="chap" select="$chap"/>
                                                    <xsl:with-param name="link" select="$title_link"/>
                                                </xsl:call-template>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:for-each>
                                </xsl:otherwise>
                                
                            </xsl:choose>

                        </div>

                    </xsl:for-each>
                </div>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="generate_chapters_list">
        <xsl:param name="link" required="yes"/>

        <a class="btn-link">
            <xsl:attribute name="href">
                <xsl:value-of select="$link"/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="@type = 'chap'">
                    <xsl:text>Chapitre </xsl:text>
                    <xsl:value-of select="@n"/>
                </xsl:when>
                <xsl:when test="@type = 'prologueProse'">
                    <xsl:text>Prologue en prose</xsl:text>
                </xsl:when>
                <xsl:when test="@type = 'prologueVerse'">
                    <xsl:text>Prologue en vers</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@type"/>
                </xsl:otherwise>
            </xsl:choose>
        </a>
        <xsl:if test="not(position() = last())">
            <xsl:text> | </xsl:text>
        </xsl:if>
    </xsl:template>


    <xsl:template name="generate_chapters_tableOfCont">
        <xsl:param name="book" required="yes"/>
        <xsl:param name="chap" required="yes"/>
        <xsl:param name="link" required="yes"/>

        <h2 class="accordion-header">
            <xsl:attribute name="id">
                <xsl:text>headingBook</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>Chap</xsl:text>
                <xsl:value-of select="$chap"/>
            </xsl:attribute>
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                aria-expanded="false">
                <xsl:attribute name="data-bs-target">
                    <xsl:text>.collapseBook</xsl:text>
                    <xsl:value-of select="$book"/>
                    <xsl:text>Chap</xsl:text>
                    <xsl:value-of select="$chap"/>
                </xsl:attribute>
                <xsl:attribute name="aria-controls">
                    <xsl:text>collapse</xsl:text>
                    <xsl:value-of select="$chap"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="@type = 'chap'">
                        <xsl:text>Chapitre </xsl:text>
                        <xsl:value-of select="@n"/>
                    </xsl:when>
                    <xsl:when test="@type = 'prologueProse'">
                        <xsl:text>Prologue en prose</xsl:text>
                    </xsl:when>
                    <xsl:when test="@type = 'prologueVerse'">
                        <xsl:text>Prologue en vers</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@type"/>
                    </xsl:otherwise>
                </xsl:choose>
            </button>
        </h2>
    </xsl:template>

    <xsl:template name="generate_titles_tableOfCont">
        <xsl:param name="value" required="yes"/>
        <xsl:param name="title" required="yes"/>
        <xsl:param name="chap" required="yes"/>
        <xsl:param name="book" required="yes"/>
        <xsl:param name="link" required="yes"/>

        <div class="accordion-collapse collapse" data-bs-parent="{concat('#accordionFlushExample',$book)}">
            <xsl:attribute name="class">
                <xsl:text>accordion-collapse collapse collapseBook</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>Chap</xsl:text>
                <xsl:value-of select="$chap"/>
            </xsl:attribute>
            <xsl:attribute name="aria-labelledby">
                <xsl:text>headingBook</xsl:text>
                <xsl:value-of select="$book"/>
                <xsl:text>Chap</xsl:text>
                <xsl:value-of select="$chap"/>
            </xsl:attribute>
            <div class="accordion-body">
                <a class="dropdown-item btn-menu">
                    <xsl:attribute name="href">
                        <xsl:value-of select="$link"/>
                    </xsl:attribute>
                    
                    <xsl:choose>
                        <xsl:when test="$value != ''">
                            <xsl:text>[</xsl:text>
                            <xsl:value-of select="$value"/>
                            <xsl:text>]</xsl:text>
                        </xsl:when>
                        
                        <!-- il y a un item ou on ne passe pas la dedans (460 "toc" contre 461 "ok") -->
                        <xsl:otherwise>
                            <xsl:choose>
                                <!-- if header : verify that current header is the good one -->
                                <xsl:when test="$title = 'true'">
                                    <xsl:choose>
                                        <xsl:when test="@corresp">
                                            <!-- 1 corresp => header = preceding page header -->
                                            <xsl:if test="not(contains(@corresp, ' '))">
                                                <span class="toc"/>
                                                <xsl:text>[</xsl:text>
                                                <xsl:apply-templates select= "preceding::tei:fw[@type='header'][1]" mode="linear"/>
                                                <xsl:text>]</xsl:text>
                                            </xsl:if>
                                        </xsl:when>
                                        <!-- 2 corresp => header = current header -->
                                        <!-- header = current header -->
                                        <xsl:otherwise>
                                            <span class="toc"/>
                                            <xsl:apply-templates select= "current()" mode="linear"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:when>
                                <!-- if not header -->
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <!-- preceding header = 1 or 2 corresp => current header = preceding header -->
                                        <xsl:when test="preceding::tei:fw[@type='header'][1]/@corresp">
                                            <span class="toc"/>
                                            <xsl:text>[</xsl:text>
                                            <xsl:apply-templates select= "preceding::tei:fw[@type='header'][1]" mode="linear"/>
                                            <xsl:text>]</xsl:text>
                                        </xsl:when>
                                        <!-- preceding header = 0 corresp => current header = following header -->
                                        <xsl:otherwise>
                                            <span class="toc"/>
                                            <xsl:choose>
                                                <!-- exist preceding header -->
                                                <xsl:when test="count(preceding-sibling::tei:fw[@type='folio']) > 0 ">
                                                    <xsl:text>[</xsl:text>
                                                    <xsl:apply-templates select= "preceding::tei:fw[@type='header'][1]" mode="linear"/>
                                                    <xsl:text>]</xsl:text>
                                                </xsl:when>
                                                <!-- not exist preceding header -->
                                                <xsl:otherwise>
                                                    <xsl:text>[</xsl:text>
                                                    <xsl:apply-templates select= "following::tei:fw[@type='header'][1]" mode="linear"/>
                                                    <xsl:text>]</xsl:text>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </a>
            </div>
        </div>
    </xsl:template>


    <!--<xsl:template match="tei:fw"> </xsl:template>-->

    <!-- plus nécessaire si pas de découpage de 1° vers/ligne -->
    <xsl:template match="tei:lb" mode="linear">
        <span class="lb"/>
    </xsl:template>

    <xsl:template match="tei:abbr | tei:expan | tei:geogName | tei:orgName | tei:placeName | tei:persName | tei:metamark | tei:fw"
        mode="linear">
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <xsl:template match="tei:rdg | tei:note" mode="linear"/>

</xsl:stylesheet>
