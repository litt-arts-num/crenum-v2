<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html [
   <!ENTITY nbsp "&amp;#160;">
]>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml"
	schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>Equipe</title>
         </titleStmt>
         <publicationStmt>
            <p>Publication Information</p>
         </publicationStmt>
         <sourceDesc>
            <p>Information about the source</p>
         </sourceDesc>
      </fileDesc>
   </teiHeader>
   <text>
      <body>
         <div>
            <head>Équipe</head>
            <div>
               <head>Un projet interdisciplinaire et international</head>
               <p>La <title>Chronique française</title> de Cretin ayant déjà suscité l’intérêt à la
                  fois d’Antoine Brix, historien spécialiste des chroniques médiévales et en
                  particulier des <title>Grandes Chroniques de France</title> ayant servi de source à
                  Cretin et d’Ellen Delvallée, chercheuse spécialiste des Rhétoriqueurs et de la
                  littérature française au tournant du Moyen Âge et de la Renaissance, le projet a été
                  conçu de manière collective et interdisciplinaire dès le début.</p>
               <p>Il s’est précisé, en 2020, grâce à une nouvelle collaboration : avec Estelle Doudet
                  (Université de Lausanne), porteuse du projet « <ref
                     target="https://medialitt.hypotheses.org/">Médialittérature</ref> », consistant à
                  étudier la formation de la culture médiatique dans les sociétés francophones au
                  tournant du Moyen Âge et de la Renaissance, de la naissance de l’imprimerie aux
                  conflits de la Réforme. La réflexion sur les principes d’édition numérique d’une
                  chronique rimée de la Renaissance a été menée en étroite collaboration avec Benedetta
                  Salvati (Université de Lausanne/École des Chartes), doctorante travaillant à
                  l’édition d’une chronique en vers contemporaine de celle de Cretin, composée à la
                  cour des ducs de Bourgogne : la <title>Chronique rimée</title> de Nicaise Ladam. À
                  terme, l’objectif est de constituer un portail d’édition des chroniques en vers
                  autour de 1500.</p>
               <p>Porté par Ellen Delvallée, le projet bénéficie du solide accompagnement de
                  l’équipe d’ingénierie en humanités numériques <ref target="https://www.elan-numerique.fr/">ELAN</ref>, au sein de l’UMR 5316
                  Litt&amp;Arts.</p>
               <p rend="bold">Le projet a reçu un financement en 2021 et 2022 dans le cadre du
                  dispositif IRGA de l’Université Grenoble Alpes.</p>
               <p>Outre la tenue de différents ateliers de travail collectif à l’UGA, ces fonds ont
                  permis la prise en charge de missions pour examiner les manuscrits de la
                  <title>Chronique</title> conservés à Paris, Chantilly, Aix-en-Provence ou au
                  Vatican. Un autre manuscrit de la <title>Chronique française</title> reste à
                  consulter à Saint-Pétersbourg. Exception faite de ce dernier, tous les manuscrits
                  ont été numérisés et sont en cours d'examen.</p>
               <p>En termes de dissémination, le projet d’édition de la
                  <title>Chronique française</title> a produit des études présentées par Antoine
                  Brix et/ou Ellen Delvallée à des conférences ou ateliers internationaux (voir la <ref target="bibliography">bibliographie</ref>). Deux colloques
                  internationaux ont par ailleurs été organisés ou coorganisés par Ellen Delvallée à
                  l’Université Grenoble Alpes pour développer la réflexion autour des enjeux de la
                  <title>Chronique</title> : <ref
                     target="https://litt-arts.univ-grenoble-alpes.fr/actualites/histoires-en-vers-xve-xvie-siecles"
                     >« Histoires en vers (XV<hi rend="superscript">e</hi>-XVI<hi rend="superscript">e</hi> siècles) »</ref>, avec Pascale Mounier en 2022,
                  ainsi que <ref
                     target="https://litt-arts.univ-grenoble-alpes.fr/actualites/guillaume-cretin-ecrivain-polygraphe"
                     >« Guillaume Cretin, écrivain polygraphe »</ref> en 2023.</p>
            </div>
            <div>
               <head>Responsables scientifiques</head>
               <list>
                  <item>Ellen Delvallée (UMR Litt&amp;Arts, CNRS/UGA), chargée de recherche spécialiste de la poésie au tournant du Moyen Âge et de la Renaissance, porteuse du projet</item>
                  <item>Antoine Brix (Université de Namur), post-doctorant en histoire, spécialiste de
                     l'historiographie vernaculaire des rois de France à la fin du Moyen Âge</item>
               </list>
            </div>
            <div>
               <head>Équipe d'ingénierie en humanités numériques</head>
               <p>L’ensemble des membres de l’équipe <ref target="https://www.elan-numerique.fr/">ELAN</ref> accompagne le projet à tous les niveaux :
                  montage du dossier de financement, formation et accompagnement des chercheurs et
                  stagiaires à l'utilisation d'outils de préparation et manipulation du corpus,
                  création d’un schéma d’encodage en xml-tei, création des visualisations de
                  données, développement de la plate-forme d’édition de la
                  <title>Chronique</title>.</p>
               <list>
                  <item>Arnaud Bey (IE UGA, Litt&amp;Arts)</item>
                  <item>Fanny Corsi (CDD IE UGA, Litt&amp;Arts ; mars-octobre 2023)</item>
                  <item>Serena Crespi (CDD IE UGA, Litt&amp;Arts ; depuis mai 2023)</item>
                  <item>Camille Desiles (CDD IE UGA, BAPSO et Litt&amp;Arts ; 2020-2021)</item>
                  <item>Anne Garcia-Fernandez (IR CNRS, Litt&amp;Arts)</item>
                  <item>Rachel Gaubil (IE UGA, Litt&amp;Arts)</item>
                  <item>Elisabeth Greslou (IR UGA, Litt&amp;Arts ; jusqu'en septembre 2023)</item>
                  <item>Théo Roulet (IE UGA, BAPSO et Litt&amp;Arts ; depuis janvier 2022)</item>
               </list>
            </div>
            <div>
               <head>Stagiaires d'édition</head>
               <p>Les étudiants stagiaires ont contribué à l’ensemble des tâches éditoriales :
                  transcription des textes, encodage structurel du document, balisage en xml-TEI
                  pour un affichage diplomatique et semi-diplomatique, repérage et identification
                  des entités nommées, création de listes de référence (header), rédaction d’un
                  manuel d’encodage.</p>
               <list>
                  <item>Fanny Corsi, étudiante de master à l'Université Grenoble Alpes, stage de 4
                     mois (mai-août 2022 et janvier-février 2023).</item>
                  <item>Nassim Mandhouj, étudiant de licence à l'Université Grenoble Alpes, stage de
                     2,5 mois (mai-juillet 2022).</item>
               </list>
            </div>
            <div>
               <head>Collaborations</head>
               <p>L’équipe « <ref target="https://medialitt.hypotheses.org/">Medialittérature.
                     Poétiques et pratiques de la communication publique en français (XVe-XVIe
                     siècles)</ref> », basée à l’université de Lausanne, est associée au projet
                  depuis sa création.</p>
               <list>
                  <item>Estelle Doudet, professeure (Université de Lausanne)</item>
                  <item>Benedetta Salvati, doctorante (Université de Lausanne/École des
                     Chartes)</item>
               </list>
            </div>
         </div>
      </body>
   </text>
</TEI>
