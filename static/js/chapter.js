window.onload = function() {
  visuInit();
  exploreInit();
  imageHandler.init();
  pageInit();

  changePath();
}

// Suppress the highlight of verses when clicking anywhere
window.onclick = function() {
  var verses = document.getElementsByClassName("highlighted-verses");
  for (var i = 0; i < verses.length; i++) {
    verses[i].classList.remove('highlighted-verses');
  }
}

// Init tabs to show : in function of the chosen visualisation (image, diplo,
// linear) for left and right tab
function visuInit() {

  // display current chapter in list chapters
  var path = window.location.pathname.split("/").slice(0, 2).join("/");
  var chapter = document.getElementById("chapName").textContent;
  path += "/" + chapter;
  document.querySelector(".btn-link[href=\"" + path + "\"]").classList.toggle("active");


  var chosenVisu = document.getElementById("visu").textContent;

  // Default values
  var leftVisu = "image-left";
  var rightVisu = "linear-right";

  if (chosenVisu != "") {
    leftVisu = chosenVisu.split("_")[0];
    rightVisu = chosenVisu.split("_")[1];
  } else {
    document.getElementById("visu").textContent = leftVisu + "_" + rightVisu;
  }

  var left = document.getElementById(leftVisu);
  var right = document.getElementById(rightVisu);
  var tabLeft = document.getElementById(leftVisu + '-tab');
  var tabRight = document.getElementById(rightVisu + '-tab');

  var toActive = [tabLeft, tabRight, left, right];
  var toShow = [left, right];

  for (var tab of toActive) {
    tab.classList.add('active');
  }
  for (var tab of toShow) {
    tab.classList.add('show');
  }
}

// Scroll to a specific page and a specific verse if they are in url
function pageInit() {
  // if page nb in url : simulate a click on thumbnail
  var pageNb = document.getElementById("pageNb").innerText;
  if (pageNb != "") {
    document.getElementById("image-left-f" + pageNb).click();
  }
  // if verseNb in url
  var verseNb = window.location.href.split("#").pop();
  if (!isNaN(verseNb)) {
    versePointer(verseNb);
  } else {
    window.onclick();
  }
}

// point the corresponding verse
function versePointer(verseNb) {
  // change url
  var newPath = window.location.pathname.split("#").slice(0, -1) + "#" + verseNb;
  let stateObj = {
    id: "url"
  };
  window.history.replaceState(stateObj, "", newPath);
  setTimeout(() => {
    // suppress ancient selected verses
    var selectedVerses = document.getElementsByClassName('highlighted-verses');
    for (var i = 0; i < selectedVerses.length; i++) {
      selectedVerses[i].classList.remove("highlighted-verses");
    }
  }, setTimeout(() => {
    // select new verse
    var verseToSelect = document.querySelector('.linear.tab-pane.show .v' + verseNb);
    verseToSelect.classList.add("highlighted-verses");
    verseToSelect.scrollIntoView({ block: "center" });
  }, "50"));
}

function activeTabs() {
  var tabs = document.getElementsByClassName("btn-tab");
  var leftTab = "";
  var rightTab = "";
  for (var i = 0; i < tabs.length; i++) {
    if (tabs[i].classList.contains("active")) {
      var id = tabs[i].id;
      if (id.split("-")[1] == "left") {
        leftTab = id.split("-")[0] + "-" + id.split("-")[1];
      } else if (id.split("-")[1] == "right") {
        rightTab = id.split("-")[0] + "-" + id.split("-")[1];
      }
    }
  }
  return [leftTab, rightTab];
}

function prevChap(chapId) {
  var tabs = activeTabs();
  var chosenVisu = tabs[0] + "_" + tabs[1];
  var newPath = window.location.pathname.split("/").slice(0, 2).join("/");
  window.location.pathname = newPath + "/" + chapId + "/" + chosenVisu;
}

function nextChap(chapId) {
  var tabs = activeTabs();
  var chosenVisu = tabs[0] + "_" + tabs[1];
  var newPath = window.location.pathname.split("/").slice(0, 2).join("/");
  window.location.pathname = newPath + "/" + chapId + "/" + chosenVisu;
}

const imageHandler = {
  viewer: Array(),
  init: function() {
    this.createPageDivs()
    // this.checkLetterLinks()
    this.countDebugMsg()
    imageHandler.viewer["left"] = new OpenSeadragon.Viewer({
      id: "openseadragon-left",
      showNavigator: false,
      showRotationControl: true,
      prefixUrl: '../static/img/libs/',
      zoomInButton: 'osd-zoom-in-left',
      zoomOutButton: 'osd-zoom-out-left',
      homeButton: 'osd-home-left',
      fullPageButton: 'osd-full-page-left',
      rotateLeftButton: 'osd-left-left',
      rotateRightButton: 'osd-right-left'
    });
    imageHandler.viewer["right"] = new OpenSeadragon.Viewer({
      id: 'openseadragon-right',
      showNavigator: false,
      showRotationControl: true,
      prefixUrl: '../static/img/libs/',
      zoomInButton: 'osd-zoom-in-right',
      zoomOutButton: 'osd-zoom-out-right',
      homeButton: 'osd-home-right',
      fullPageButton: 'osd-full-page-right',
      rotateLeftButton: 'osd-left-right',
      rotateRightButton: 'osd-right-right'
    });
    const thumbnails = document.querySelectorAll('.img-thumbnail');
    [].forEach.call(thumbnails, function(element) {
      element.addEventListener("click", imageHandler.change);
    });

    imageHandler.display("left", thumbnails[0], false);
    imageHandler.display("right", thumbnails[0], false);

    window.onscroll = function() {
      let body = document.querySelector('body');
      if (window.scrollY >= 300) {
        body.classList.add("scrolled");
      } else {
        body.classList.remove("scrolled");
      }
    };

    let modalCalls = document.querySelectorAll('.modal-call');
    [].forEach.call(modalCalls, function(modalCall) {
      modalCall.addEventListener("click", imageHandler.modalCall);
    });

    let toggleBtns = document.querySelectorAll('.toggle-mark');
    [].forEach.call(toggleBtns, function(toggleBtn) {
      toggleBtn.addEventListener("click", imageHandler.toggleMark);
    });
  },
  // checkLetterLinks: function() {
  //   const publishedIds = JSON.parse(document.getElementById('letter').dataset.publishedids)
  //   let links = document.querySelectorAll('a[data-cp-id]');
  //   [].forEach.call(links, function(link) {
  //     if (!publishedIds.includes(link.dataset.cpId)) {
  //       link.setAttribute('href', "javascript: void(0)")
  //       link.classList.add("disabled")
  //     }
  //   });
  // },
  toggleMark: function(evt) {
    let btn = evt.currentTarget;
    let pos = btn.dataset.pos;
    let type = btn.dataset.type;
    // console.log(document.querySelector('#nav-tabContent-' + pos));

    document.querySelector('#nav-tabContent-' + pos).classList.toggle('disabled-' + type);
  },
  modalCall: function(evt) {
    evt.stopPropagation();
    let modalCall = evt.currentTarget;
    let modalId = modalCall.dataset.modalId;

    let modalNb = document.querySelector('[modal-title="' + modalId + '"]').innerHTML;
    let modalText = document.querySelector('[modal-text="' + modalId + '"]').innerHTML;
    document.querySelector("#modal .modal-text").innerHTML = modalText;
    document.querySelector("#modal-title-id").innerHTML = modalNb;

    new bootstrap.Modal(document.getElementById("modal"), {}).toggle();
  },
  change: function(evt) {
    let previous;
    let thumbnail = evt.currentTarget;
    let pos = thumbnail.dataset.pos;
    let url = thumbnail.dataset.img;

    if (previous = document.querySelector('#nav-tabContent-' + pos + ' .img-thumbnail.current')) {
      previous.classList.remove('current');
    }
    thumbnail.classList.add('current');

    imageHandler.display(pos, thumbnail, true);

    changePath();
  },

  display: function(where, thumbnail, scroll) {
    let url = thumbnail.dataset.img;
    let position = thumbnail.id.substr(thumbnail.id.lastIndexOf("-") + 1);

    if (url) {
      imageHandler.viewer[where].open({
        type: 'image',
        url: url
      });
    }

    this.colorPageDiv(position, scroll);
  },
  countDebugMsg: function() {
    const count = document.querySelectorAll('#left-panel .debug-info').length;
    if (count > 0) {
      for (var el of document.querySelectorAll('.debug-related')) {

        el.classList.remove('d-none');
      }
      for (var el of document.querySelectorAll('.bug-count')) {
        el.innerHTML = count;
      }
    }

  },
  changeImg: function(evt) {
    let previous;
    let target = evt.target;
    let div = target.closest('[data-img-id]');
    let id = div.getAttribute('data-img-id');

    if (previous = document.querySelector('.img-thumbnail.current')) {
      previous.classList.remove('current');
    }

    let leftThumbnail = document.querySelector('#image-left-' + id);
    let rightThumbnail = document.querySelector('#image-right-' + id);

    imageHandler.display("left", leftThumbnail, false);
    imageHandler.display("right", rightThumbnail, false);

    if (previous = document.querySelector('#nav-tabContent-left .img-thumbnail.current')) {
      previous.classList.remove('current');
    }
    if (previous = document.querySelector('#nav-tabContent-right .img-thumbnail.current')) {
      previous.classList.remove('current');
    }
    leftThumbnail.classList.add('current');
    rightThumbnail.classList.add('current');
    changePath();
  },
  removePageDivColor: function() {
    let pageTextDivs = document.querySelectorAll('.current-page-text');
    for (var pageTextDiv of pageTextDivs) {
      pageTextDiv.classList.remove('current-page-text');
    }
  },
  colorPageDiv: function(position, scroll) {
    this.removePageDivColor();
    let pageTextDivs = document.querySelectorAll(`div[data-img-id="${position}"]`);
    for (var pageTextDiv of pageTextDivs) {
      pageTextDiv.classList.add('current-page-text');
    }
    if (scroll) {
      let currentPageDiv = document.querySelector('.tab-pane.show div.current-page-text');
      if (currentPageDiv) {
        currentPageDiv.scrollIntoView(true);
      }
    }
  },
  createPageDivs: function() {
    const hrs = document.querySelectorAll('hr');
    for (var hr of hrs) {
      let next = hr.nextSibling;
      let div = document.createElement('div');
      hr.parentNode.insertBefore(div, next);
      div.classList.add('page-div');
      div.setAttribute('data-img-id', hr.getAttribute('data-img-id'));
      div.addEventListener("click", imageHandler.changeImg);
      while (next) {
        let node = next;
        next = next.nextSibling;
        div.appendChild(node);
        if (!next || next.nodeName == 'HR') break;
      }
    }
  }
}

// get current path, chapter, page and visu informations to change url
function changePath() {
  var ancientPath = window.location.href;
  var newPath = window.location.pathname.split("/").slice(0, 2).join("/");
  var visuLeft = document.getElementById("nav-tab-left").getElementsByClassName("nav-link active")[0].getAttribute("aria-controls");
  var visuRigth = document.getElementById("nav-tab-right").getElementsByClassName("nav-link active")[0].getAttribute("aria-controls");
  var visu = visuLeft + "_" + visuRigth;
  document.getElementById("visu").textContent = visu;
  var page = document.getElementsByClassName("current-page-text")[0].getAttribute("data-img-id").replace("f", "p");
  var chapter = document.getElementById("chapName").textContent;

  // if verseNb in url
  var verseNb = ancientPath.split("#").pop();
  if (!isNaN(verseNb)) {
    var verses = document.getElementsByClassName("v" + verseNb);
    for (var i = 0; i < verses.length; i++) {
      verses[i].classList.add('highlighted-verses');
    }
    newPath += "/" + chapter + "/" + page + "/" + visu + "#" + verseNb;
  } else {
    window.onclick();
    newPath += "/" + chapter + "/" + page + "/" + visu;
  }

  let stateObj = {
    id: "url"
  };
  window.history.replaceState(stateObj, "", newPath);
}
