let handler = {
  listPersons: "../static/json/list_persons.json",
  listRelations: "../static/json/list_relations.json",
  listPersonsRelations: Array(),
  personsNames: Object(),
  personsRelations: Object(),
  style: [{
      selector: 'node',
      style: {
        'label': 'data(name)'
      }
    },
    {
      selector: 'edge',
      style: {
        'label': 'data(name)',
        'font-size': '12rem',
        'font-style': 'italic',
        'width': 3,
        'line-color': '#ccc',
        'target-arrow-color': '#ccc',
        'target-arrow-shape': 'triangle',
        'curve-style': 'bezier',
        'padding': 50,
        'text-wrap': 'wrap'
      }
    }
  ],
  layout: {
    name: 'elk', // type of graph
    nodeDimensionsIncludeLabels: true, // Boolean which changes whether label dimensions are included when calculating node dimensions
    zoom: 1,
    pan: {
      x: 0,
      y: 0
    }, // start location for graph
    // fit: true, // Whether to fit
    // padding: 20, // Padding on fit
    animate: false, // Whether to transition the node positions
    animateFilter: function(node, i) {
      return true;
    }, // Whether to animate specific nodes when animation is on; non-animated nodes immediately go to their final positions
    animationDuration: 500, // Duration of animation in ms if enabled
    animationEasing: undefined, // Easing of animation if enabled
    transform: function(node, pos) {
      return pos;
    }, // A function that applies a transform to the final node position
    ready: undefined, // Callback on layoutready
    stop: undefined, // Callback on layoutstop
    elk: {
      // All options are available at http://www.eclipse.org/elk/reference.html

      'algorithm': 'layered', // style of elk graph
      'elk.direction': 'DOWN', // vertical graph

      'elk.layered.spacing.nodeNodeBetweenLayers': 100, // The spacing to be preserved between any pair of nodes of two adjacent layers.
      'elk.layered.nodePlacement.strategy': 'NETWORK_SIMPLEX', // Strategy for node placement.
      // 'elk.spacing.edgeEdge':50, // Spacing to be preserved between any two edges.
      'elk.spacing.edgeNode': 200, // Spacing to be preserved between nodes and edges.
      'elk.layered.spacing.edgeNodeBetweenLayers': 50, // The spacing to be preserved between nodes and edges that are routed next to the node’s layer - agrandit les edges
      'elk.spacing.nodeNode': 100, // The minimal distance to be preserved between each two nodes.

      // 'elk.layered.wrapping.strategy':'SINGLE_EDGE',

    },
    priority: function(edge) {
      return null;
    }, // Edges with a no-null value are skipped when geedy edge cycle breaking is enabled
  },
  init: function() {

    // Get list persons and relations
    $.get(handler.listPersons, function(persons) {
      for (var i = 0; i < persons.length; i++) {
        handler.personsNames[persons[i]["id"]] = persons[i];
      }
      $.get(handler.listRelations, function(relations) {
        for (var i = 0; i < relations.length; i++) {
          if (!(relations[i]["id"] in handler.personsRelations)) {
            handler.personsRelations[relations[i]["id"]] = relations[i];
          }
          // Manage 2 different relation types btwn 2 same persons
          else {
            handler.personsRelations[relations[i]["id"]]["name"] += " et\n " + relations[i]["name"];
          }
        }
        // Add persons as nodes and relations as edges for graph
        for (var j in handler.personsRelations) {
          handler.listPersonsRelations.push({
            group: 'nodes',
            data: handler.personsNames[handler.personsRelations[j]["source"]]
          });
          handler.listPersonsRelations.push({
            group: 'nodes',
            data: handler.personsNames[handler.personsRelations[j]["target"]]
          });
          handler.listPersonsRelations.push({
            group: "edges",
            data: handler.personsRelations[j]
          });
        }

        // If little screen reduce base zoom
        var windowWidth = window.innerWidth;
        if (windowWidth < 1000){
          handler.layout["zoom"] = 0.5;
        }
        else if (windowWidth < 1200){
          handler.layout["zoom"] = 0.7;
        }
        else if (windowWidth < 1400){
          handler.layout["zoom"] = 0.85;
        }

        // Create graph
        var cy = cytoscape({
          container: document.getElementById('graph'),
          elements: handler.listPersonsRelations,
          style: handler.style,
          layout: handler.layout,
          wheelSensitivity: 0.5
        });

        // Resize container in function of graph size
        cy.container().style.height = "2250px";
        cy.resize();

        // Zoom on a specific node and its childs
        cy.unbind("click");
        cy.bind("click", "node", function(event) { // just for demonstration  purposes here
          var coll = cy.$(event.target).successors(); // get all outgoing nodes
          coll = coll.add(event.target); // add their source
          var removed = cy.remove(cy.elements().not(coll)); // remove all other elements
          var len = cy.nodes().length;
          var pad = (len < 10 ? (len < 5 ? (len < 3 ? (len < 2 ? 150 : 100) : 75) : 50) : 25); // custom padding function here
          cy.animate({
            fit: {
              eles: cy.elements(),
              padding: pad
            }
          }, {
            duration: 500,
            easing: 'ease-in'
          });
        });

        // Add an event when clicking on node
        // cy.on('click', 'node', function(event) {
        //   var node = event.target;
        //   console.log('tapped ' + node.id());
        // });

        // the default values of each option are outlined below:
        var defaults = {
          zoomFactor: 0.05, // zoom factor per zoom tick
          zoomDelay: 45, // how many ms between zoom ticks
          minZoom: 0.1, // min zoom level
          maxZoom: 10, // max zoom level
          fitPadding: 0, // padding when fitting
          zoomOnly: true, // a minimal version of the ui only with zooming (useful on systems with bad mousewheel resolution)
          fitSelector: undefined, // selector of elements to fit
          animateOnFit: function() { // whether to animate on fit
            return false;
          },
          fitAnimationDuration: 1000, // duration of animation on fit

          // icon class names
          zoomInIcon: 'fa fa-plus',
          zoomOutIcon: 'fa fa-minus',
          resetIcon: 'fa fa-expand'
        };

        // add the panzoom control
        cy.panzoom(defaults);

        // Add refresh bttn
        panzoom = document.getElementsByClassName("cy-panzoom")[0];
        var hr = document.createElement("hr");
        panzoom.appendChild(hr);
        div = document.createElement("div");
        div.classList.add("cy-panzoom-zoom-button");
        span = document.createElement("span");
        span.classList.add("icon");
        span.classList.add("fa");
        span.classList.add("fa-rotate");
        span.setAttribute("onclick","window.location.reload()");
        div.appendChild(span);
        panzoom.appendChild(div);

      });
    });
  }
}

window.onload = function() {
  // Stop fix navbar + hide footer
  exploreInit();
  handler.init();
}
